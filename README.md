# phpMyAdmin

This is an app that is using a docker image with `apache-php` and creating a phpMyAdmin application that we are connecting it with the docker-compose with a database. 


We are using the latest phpMyAdmin version at the moment which is **5.1.0**. We are also installing required packages that make phpMyAdmin run correctly. 

We have set some necessary environment variables `PMA_HOST` and `PMA_DB`. The first one is being used to set the host that we are going to connect from. This value can't be localhost but a valid private or public IP Address otherwise we won't be able to access phpMyAdmin from a browser. The second one is the **domain name** or **IP Address** of the database that we are connecting phpMyAdmin to. 

Furthermore we have set some optional variables such as `SECURITY_ENABLED` and `ALIAS`. These two variables must be both set if we want to enable security in our machine. `ALIAS` is being used to set the URL that we will type which could look like `http://IP_ADDRESS/ALIAS`.

To use the `docker-compose.yml` file it is mandatory to create a `.env` file where we will add every environment variable so we don't have it as a plain text in our `docker-compose.yml`. 

To do so we move into the phpMyAdmin directory and use the below commands

    vim .env

And we add the below configuration like the one we show 

    PMA_DB=
    PMA_HOST=
    SSL_ENABLED=
    SECURITY_ENABLED=
    ALIAS=
    MYSQL_ROOT_PASSWORD=
    MYSQL_USER=
    MYSQL_PASSWORD=
    MYSQL_DATABASE=

We haven't set these variables any value but we should. 

- `SSL_ENABLED` and `SECURITY_ENABLED` are true/false variables.
- `PMA_HOST` is a variable that can accept a single IP or a IP network. This IP Address can be a private IP Address or a public one. ( set it to `192.168.0.1` for a single IP or `192.168.1.0/24` for a whole network )
- `PMA_DB` is the domain name or the IP address of the remote database that we will connect to. 
- `ALIAS` is an alphanumeric string that we use to change the URL alias.
- `MYSQL_ROOT_PASSWORD` is used to set the root password for mariadb.
- `MYSQL_USER` is used to set the name of a mariadb user that we are creating. ( it should be noted that MYSQL_PASSWORD must be set as well to create a user ) 
- `MYSQL_PASSWORD` as we already explained is the user's password.
- `MYSQL_DATABASE` is a database name that we are creating. ( If a user and a password are also set then this user will have all privileges on that database )   




