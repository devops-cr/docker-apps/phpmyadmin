FROM apache-php:7-christos

RUN mkdir -p /var/www/html/phpmyadmin \
 && yum install -y php-xml php-mbstring \
 && curl -fsSL -o phpMyAdmin.tar.gz https://files.phpmyadmin.net/phpMyAdmin/5.1.0/phpMyAdmin-5.1.0-all-languages.tar.gz \
 && tar -xf phpMyAdmin.tar.gz -C /var/www/html/phpmyadmin --strip-components=1 \
 && mkdir -p /var/www/html/phpmyadmin/tmp \
 && chmod 777 /var/www/html/phpmyadmin/tmp \
 && rm -rf phpMyAdmin.tar.gz /var/www/html/phpmyadmin/setup /var/www/html/phpmyadmin/test /var/www/html/index.php \
 && yum clean all

ENV \
    SECURITY_ENABLED=false

COPY etc/ /etc/


